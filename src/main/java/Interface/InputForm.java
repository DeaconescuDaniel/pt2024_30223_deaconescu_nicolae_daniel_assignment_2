package Interface;

import Model.Client;
import Model.Queue;
import Utility.Simulator;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;


public class InputForm extends JDialog {
    private JTextField clientNumField;
    private JTextField queueNumField;
    private JTextField maxTimeField;
    private JTextField minArrivalField;
    private JTextField maxArrivalField;
    private JTextField minServiceField;
    private JTextField maxServiceField;
    private JButton submitButton;
    private JPanel contentPanel;
    private JPanel InputPanel;
    private Integer numQueues;
    private Integer numClients;
    private Integer maxTime;
    private Integer minArrival;
    private Integer maxArrival;
    private Integer minService;
    private Integer maxService;

    public InputForm(JFrame parent) {
        super(parent);
        setTitle("Queue initialization");
        setContentPane(contentPanel);
        setMinimumSize(new Dimension(550, 400));
        setModal(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        submitButton.addActionListener(e -> {
            try {
                numClients = Integer.parseInt(clientNumField.getText());
                numQueues = Integer.parseInt(queueNumField.getText());
                maxTime = Integer.parseInt(maxTimeField.getText());
                minArrival = Integer.parseInt(minArrivalField.getText());
                maxArrival = Integer.parseInt(maxArrivalField.getText());
                minService = Integer.parseInt(minServiceField.getText());
                maxService = Integer.parseInt(maxServiceField.getText());
                if (numQueues <= 0 || numClients <= 0 || maxTime <= 0 || minArrival >= maxArrival || minService >= maxService || minArrival <= 0 || minService <= 0) {
                    throw new RuntimeException();
                }
            } catch (Exception exception) {
                JOptionPane.showMessageDialog(parent, "Invalid input", "Error", JOptionPane.ERROR_MESSAGE);
            }
            dispose();
            List<Client> clientList=Collections.synchronizedList(new ArrayList<>());
            for(int i=0;i<numClients;i++){
                clientList.add(Client.generateRandomClient(minArrival,maxArrival,minService,maxService));
            }
            Collections.sort(clientList);
            List<Queue> queueMap=Collections.synchronizedList(new ArrayList<>());
            for(int i=0;i<numQueues;i++){
                queueMap.add(new Queue());
            }
            try {
                new Simulator(clientList,queueMap,maxTime);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }

        });
        setVisible(true);
    }

}
