package Utility;

import Model.Client;
import Model.Queue;
import java.util.List;


public class LogEntry{
    private static int time = 0;
    private final List<Client> waitingClients;
    private final List<Queue> queues;

    public LogEntry(List<Client> waitingClients, List<Queue> queues) {
        this.waitingClients = waitingClients;
        this.queues = queues;
        time++;
    }

    @Override
    public String toString(){
        StringBuilder s= new StringBuilder("Time: ");
        s.append(time);
        s.append("\n");
        s.append("Waiting clients: ");
        for(Client c:waitingClients){
            s.append(c.toString());
            s.append(",");
        }
        s.append("\n");
        for(Queue q:queues){
            s.append("Queue ");
            s.append(q.getClientId());
            s.append(":");
            for (Client c:q.getClientsInQueue()){
                s.append(c);
                s.append(",");
            }
            s.append("\n");
        }
        s.append("\n");
        return s.toString();
    }
}
