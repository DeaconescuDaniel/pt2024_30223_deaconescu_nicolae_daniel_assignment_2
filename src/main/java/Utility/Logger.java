package Utility;

import java.io.*;

public class Logger {
    private static final String filename="log.txt";

    public synchronized static void writeLog(LogEntry entry){
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true))) {
            writer.write(entry.toString());
            writer.newLine();
        } catch (IOException e) {
            System.err.println("An error occurred: " + e.getMessage());
        }
    }
    public synchronized static void writeAverageWaitingTime(Double averageWaitingTime){
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true))) {
            writer.newLine();
            writer.write("Average waiting time: " + averageWaitingTime.toString());
        } catch (IOException e) {
            System.err.println("An error occurred: " + e.getMessage());
        }
    }
}
