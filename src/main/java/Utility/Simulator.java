package Utility;

import Model.Client;
import Model.Queue;
import Scheduling.SchedulingService;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import static java.lang.Thread.sleep;

public class Simulator {
    private final List<Queue> queues;
    private final Integer numClients;
    private Double averageWaitingTime = 0.0;

    public Simulator(List<Client> clientList, List<Queue> queues, Integer maxTime) throws Exception {
        this.numClients = clientList.size();
        this.queues=queues;
        int currentTime = 0;
        for (Queue q : queues) {
            q.start();
        }
        AtomicInteger i = new AtomicInteger(0);
        do {
            currentTime += 1000;
            Iterator<Client> iterator = clientList.iterator();
            while (iterator.hasNext()) {
                Client c = iterator.next();
                if (c.getArrivalTime() * 1000 == currentTime) {
                    SchedulingService.processClient(c, queues);
                    iterator.remove();
                } else {
                    break;
                }
            }
            LogEntry entry = new LogEntry(clientList, queues);
            Logger.writeLog(entry);
            System.out.println(currentTime);
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            i.getAndIncrement();
        } while (i.get() < maxTime && !(clientList.isEmpty() && queuesEmpty()));
        stopQueues();
    }

    private boolean queuesEmpty(){
        for (Queue q : queues) {
            if (!q.getClientsInQueue().isEmpty()) {
                return false;
            }
        }
        return true;
    }
    private void stopQueues(){
        for (Queue q : queues) {
            q.stopQueue();
            averageWaitingTime += q.getReturnedTime();
        }
        averageWaitingTime /= numClients;
        Logger.writeAverageWaitingTime(averageWaitingTime);
        System.out.println("Average waiting time: " + averageWaitingTime);
    }
}
