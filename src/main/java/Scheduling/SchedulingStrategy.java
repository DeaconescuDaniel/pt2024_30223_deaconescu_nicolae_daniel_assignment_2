package Scheduling;

import Model.Client;
import Model.Queue;

import java.util.List;

public interface SchedulingStrategy {
    void processClient(Client c, List<Queue> queues) throws Exception;
}
