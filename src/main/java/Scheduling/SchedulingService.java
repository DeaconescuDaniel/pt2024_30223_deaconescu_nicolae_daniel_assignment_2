package Scheduling;

import Model.Client;
import Model.Queue;
import java.util.List;


public class SchedulingService {
    static SchedulingStrategy strategy;

    public static void processClient(Client c, List<Queue> queues) throws Exception {
        int numClients = queues.get(0).getNumclientsInQueue();
        for(Queue q : queues){
            if(q.getNumclientsInQueue() != numClients){
                strategy=new SmallestQueue();
                try {
                    strategy.processClient(c,queues);
                }catch(Exception e){
                    e.printStackTrace();
                }
                return;
            }
        }
        strategy = new ShortestTime();
        try {
            strategy.processClient(c, queues);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
