package Scheduling;

import Model.Client;
import Model.Queue;

import java.util.ArrayList;
import java.util.List;

public class SmallestQueue implements SchedulingStrategy{

    @Override
    public void processClient(Client c, List<Queue> queues) throws Exception {
        int currentMin=Integer.MAX_VALUE;
        int minWaiting=Integer.MAX_VALUE;
        Queue minQueue=null;
        ArrayList<Queue> candidates=new ArrayList<>();
        for(Queue q : queues) {
            if(q.getClientsInQueue().size() < currentMin) {
                currentMin = q.getClientsInQueue().size();
                minQueue = q;
                candidates.clear();
                candidates.add(minQueue);
            }else if(q.getClientsInQueue().size() == currentMin) {
                candidates.add(q);
            }
        }
        for(Queue q : candidates) {
            if(q.getCurrentWaitingTime() < minWaiting) {
                minWaiting = q.getCurrentWaitingTime();
                minQueue = q;
            }
        }
        if(minQueue != null) {
            minQueue.addClient(c);
        }else{
            throw new Exception("Error processing client, check smallestQueue");
        }
    }
}
