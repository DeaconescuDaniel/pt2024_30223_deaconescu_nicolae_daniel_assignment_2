package Scheduling;

import Model.Client;
import Model.Queue;

import java.util.List;

public class ShortestTime implements SchedulingStrategy{
    @Override
    public void processClient(Client c, List<Queue> queues) throws Exception {
        int shortestTime = Integer.MAX_VALUE;
        int currentTime = 0;
        Queue shortestQueue = null;
        for(Queue q : queues) {
            for (Client client : q.getClientsInQueue()) {
                currentTime += client.getServiceTime();
            }
            if (currentTime < shortestTime) {
                shortestTime = currentTime;
                shortestQueue = q;
            }
        }
        if (shortestQueue != null) {
            shortestQueue.addClient(c);
        }else{
            throw new Exception("Error processing client, check ShortestTime");
        }
    }
}
