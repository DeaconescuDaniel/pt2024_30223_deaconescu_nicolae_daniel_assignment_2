package Model;

import java.util.Random;

public class Client implements Comparable<Client> {
    private static int clientsNumber = 1;
    private final Integer id;
    private final Integer arrivalTime;
    private Integer serviceTime;

    public Client(Integer arrivalTime, Integer serviceTime) {
        this.id = clientsNumber++;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
    }

    public Integer getArrivalTime() {
        return arrivalTime;
    }

    public Integer getServiceTime() {
        return serviceTime;
    }

    public synchronized void decrementServiceTime() {
        if(serviceTime != 0) {
            serviceTime--;
        }
    }
    public static Client generateRandomClient(int minArrival, int maxArrival, int minService, int maxService) {
        Random random = new Random();
        int arrivalTime = random.nextInt(maxArrival - minArrival + 1) + minArrival;
        int serviceTime = random.nextInt(maxService - minService + 1) + minService;
        return new Client(arrivalTime, serviceTime);
    }

    @Override
    public String toString() {
        return "(" + id + "," + arrivalTime + "," + serviceTime + ')';
    }


    @Override
    public int compareTo(Client o2) {
        if (this.arrivalTime > o2.arrivalTime) {
            return 1;
        } else if (this.arrivalTime < o2.arrivalTime) {
            return -1;
        }
        return 0;
    }
}
