package Model;

import java.util.ArrayList;

public class Queue extends Thread implements Comparable<Queue> {
    private static Integer numQueues = 1;
    private final Integer id;
    private Double returnedTime=0.0;
    private Integer currentWaitingTime;         //Note, this is not the data that will be written to the log, it is the sum of all waiting times in the queue
    private Integer numclientsInQueue;
    private final ArrayList<Client> clientsInQueue;
    private volatile boolean running=true;

    public Queue() {
        this.id = numQueues++;
        this.numclientsInQueue = 0;
        this.currentWaitingTime = 0;
        this.clientsInQueue = new ArrayList<>();
    }

    @Override
    public int compareTo(Queue o) {
        if (this.currentWaitingTime > o.currentWaitingTime) {
            return 1;
        } else if (this.currentWaitingTime < o.currentWaitingTime) {
            return -1;
        }
        return 0;
    }

    public void stopQueue(){
        this.running = false;
    }

    @Override
    public void run() {
        while (running) {
            if (!clientsInQueue.isEmpty()) {
                returnedTime+=clientsInQueue.size();
                if (clientsInQueue.get(0).getServiceTime() == 0) {
                    clientsInQueue.remove(0);
                }
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                synchronized (this) {
                    try {
                        clientsInQueue.get(0).decrementServiceTime();
                    } catch (Exception ignored) {

                    }
                }
            }
        }
    }

    public Double getReturnedTime() {
        return returnedTime;
    }

    public synchronized void addClient(Client client) {
        this.clientsInQueue.add(client);
        this.currentWaitingTime += client.getServiceTime();
        this.numclientsInQueue++;
    }

    public ArrayList<Client> getClientsInQueue() {
        return clientsInQueue;
    }

    public Integer getClientId() {
        return id;
    }

    public Integer getNumclientsInQueue() {
        return numclientsInQueue;
    }

    public Integer getCurrentWaitingTime() {
        return currentWaitingTime;
    }

}
